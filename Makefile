CC=cc
OPT=-Os
CFLAGS=-Wall -Wpedantic

build:
	$(CC) $(OPT) $(CFLAGS) -o chi chi.c tomas-termbox/*.c

debug:
	$(CC) -ggdb -D DEBUG $(CFLAGS) -o chi chi.c tomas-termbox/*.c

