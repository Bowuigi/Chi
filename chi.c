/*
 * Chi: A Terminal based CHIP-8 emulator (interpreter, actually)
 * Inspired by https://tobiasvl.github.io/blog/write-a-chip-8-emulator/
 * Uses tomas' fork of Termbox: https://github.com/tomas/termbox
*/

// Integers and booleans to use memory more efficiently
#include <stdbool.h>
#include <stdint.h>

// Some utility memory copying functions
#include <string.h>

// random/srandom number generator functions from POSIX
#include <stdlib.h>

// Getting the time in seconds (to seed the RNG)
#include <time.h>

// Logging
#include <stdio.h>

// The source code for tomas version of Termbox
#include "tomas-termbox/termbox.h"

// 4kb of RAM
#define RAM_AMOUNT 4096
static uint8_t RAM[RAM_AMOUNT];

// Originally this was the space after the interpreter, back in the COSMAC-VIP
#define INITIAL_OFFSET 0x200

// Small font
#define FONT_START 0x50

// Macro that makes error handling a bit easier
#define CHI_ERR(msg) do {\
	fprintf(stderr, "Chi, fatal error: %s\n", msg);\
	tb_shutdown();\
	exit(1);\
} while (0)

// 64x32 monochrome screen
#define SCREEN_WIDTH 64
#define SCREEN_HEIGHT 32

static bool Screen[SCREEN_HEIGHT][SCREEN_WIDTH] = {false};

// Program Counter, points to a location in memory and executes it (requires
// at least 12 bits)
static uint16_t PC;

// Used to point to locations in memory (therefore requiring at least 12 bits)
static uint16_t IPointer = 0;

// An array that works like a stack, used to call and return from functions
static uint16_t CallStack[16];

// Index that points to the top value of CallStack
static uint8_t SP = 0;

// General timer, decrements at 60Hz (60 times a second)
static uint8_t DelayTimer = 0;

// Like the Delay Timer but plays a sound if it is not zero
static uint8_t SoundTimer = 0;

// 16 8 bit Registers
static uint8_t Register[16];

// Macro to modify the carry+collision+underflow flag (VF)
#define Flag Register[15]

// Fonts
static uint8_t Font[] = {
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

// COSMAC-VIP layout (also used on HP48 and modern interpreters)
// 1 2 3 C
// 4 5 6 D
// 7 8 9 E
// A 0 B F
//
// Dream 6800 and ETI-660 layout
// 0 1 2 3
// 4 5 6 7
// 8 9 A B
// C D E F
//
static bool Keys[16] = {false};

// Should work for most CHIP-8 games
static unsigned int InstrPerSecond = 1; // 700

// true if we should use COSMAC-VIP's version of shifts
static bool ShiftQuirk = false;

// true if we should use SCHIP's version of Jump with offset
static bool JOffsetQuirk = false;

// true if we should set the Register F if I overflows in Add to index
static bool Add2IndexQuirk = true;

// true if we should increment the value of I in save and load instructions
static bool SaveLoadQuirk = false;

// Masks for getting the needed N numbers, I is for instruction
#define NIII(i) ((i & 0xF000) >> 12)
#define INII(i) ((i & 0x0F00) >> 8)
#define IINI(i) ((i & 0x00F0) >> 4)
#define IIIN(i) (i & 0x000F)
#define IINN(i) (i & 0x00FF)
#define INNN(i) (i & 0x0FFF)

#define VX Register[INII(inst)]
#define VY Register[IINI(inst)]

// Drawing code

static tb_color DisplayOn = TB_WHITE;
static tb_color DisplayOff = TB_DARKEST_GREY;
static tb_chr TermPixel = 0x2580; // Half block, actually represents two pixels

static void redraw() {
	for (uint8_t y = 0, ypx = 0; y < (SCREEN_HEIGHT / 2); y++, ypx += 2) {
		for (uint8_t x = 0; x < SCREEN_WIDTH; x++) {
#ifndef DEBUG
			tb_char(x, y, Screen[ypx][x] ? DisplayOn : DisplayOff, Screen[ypx+1][x] ? DisplayOn : DisplayOff, TermPixel);
#else
			fprintf(stderr, "Pixel %dx%d is %s\n", x, y, Screen[y][x] ? "on" : "off");
			fprintf(stderr, "Pixel %dx%d is %s\n", x, y+1, Screen[y+1][x] ? "on" : "off");
#endif
		}
	}

	tb_render();
}

static void clear_screen() {
	for (uint8_t y = 0; y < SCREEN_HEIGHT; y++) {
		for (uint8_t x = 0; x < SCREEN_HEIGHT; x++) {
			Screen[y][x] = false;
		}
	}
}

static void CS_Push(uint16_t value) {
	if (SP >= 16) {
		CHI_ERR("Stack overflow!");
	}

	CallStack[SP] = value;
	SP++;
}

static uint16_t CS_Pop() {
	SP--;
	uint16_t n = CallStack[SP];
	CallStack[SP] = 0;
	return n;
}

int main(int argc, char **argv) {
	// Initialization stage
	
	// Set the time to wait between instructions
	struct timespec wait = {
		.tv_sec = 0,
		.tv_nsec = 1e18
	};

	argc--;

	// Read a filename from the first argument
	if (argc >= 1) {
		FILE *romfile = fopen(argv[1], "r");

		if (!romfile) {
			perror("Chi, fatal error");
		}

		int byte = fgetc(romfile);

		if (feof(romfile)) {
			fclose(romfile);
			fprintf(stderr, "Chi, fatal error: Empty file\n");
			return 1;
		} else if (ferror(romfile)) {
			fclose(romfile);
			fprintf(stderr, "Chi, fatal error: Failed to read file\n");
		}

		uint16_t index;

		for (index = 0; byte != EOF; byte = fgetc(romfile), index++) {
			RAM[INITIAL_OFFSET + index] = (uint8_t)byte;
		}

		if (ferror(romfile)) {
			fclose(romfile);
			fprintf(stderr, "Chi, fatal error: Failed to read file\n");
		}

	} else {
		fprintf(stderr, "Chi: Terminal CHIP-8 emulator, usage: chi rom.ch8\n");
		return 1;
	}
	
	// Copy the font to memory
	(void) memcpy(RAM + FONT_START - 1, Font, sizeof Font);

	// Seed the random number generator
	srandom((unsigned int)time(NULL));

	if (tb_init() != 0) {
		fprintf(stderr, "Chi, fatal error: Could not initialize tomas/termbox\n");
		return 1;
	}

	struct tb_event term_ev;

	redraw();

	// Current instruction
	uint16_t inst;

	// Work on the whole RAM
	for (PC = INITIAL_OFFSET; PC < RAM_AMOUNT; PC += 2) {

		// Fetching stage
		inst = RAM[PC];
		inst <<= 8;
		inst += RAM[PC + 1];

#ifndef DEBUG
		// Graphic debugging
		tb_stringf(SCREEN_WIDTH + 2, 1, DisplayOn, TB_DEFAULT, "I %x PC %x Instruction %X", IPointer, PC, (RAM[PC] << 8) + RAM[PC + 1]);
		tb_stringf(SCREEN_WIDTH + 2, 2, DisplayOn, TB_DEFAULT, "Registers: v0=%x v1=%x v2=%x", Register[0], Register[1], Register[2]);
		tb_stringf(SCREEN_WIDTH + 2, 3, DisplayOn, TB_DEFAULT, "v3=%x v4=%x v5=%x v6=%x v7=%x", Register[3], Register[4], Register[5], Register[6], Register[7]);
		tb_stringf(SCREEN_WIDTH + 2, 4, DisplayOn, TB_DEFAULT, "v8=%x v9=%x vA=%x vB=%x vC=%x", Register[8], Register[9], Register[10], Register[11], Register[12]);
		tb_stringf(SCREEN_WIDTH + 2, 5, DisplayOn, TB_DEFAULT, "vD=%x vE=%x vF=%x", Register[13], Register[14], Register[15]);
		tb_render();
#endif

		// Execution stage
		switch (NIII(inst)) {
			case 0x0:
				// 0x0NNN: Execute machine instruction, only some are implemented because
				// they are machine specific
				switch (INNN(inst)) {
					case 0x000:
//						CHI_ERR("Reached uninitialized memory!");
						break;
					case 0x0E0:
						// 0x00E0 Clear screen
#ifndef DEBUG
						clear_screen();
#endif
						break;
					case 0x0EE:
						// 0x00EE: Return from subroutine
						PC = CS_Pop();
						break;
				}
				break;
			case 0x1:
				// 0x1NNN: Jump
				PC = INNN(inst) - 1;
				break;
			case 0x2:
				// 0x2NNN: Call subroutine
				CS_Push(INNN(inst));
				break;
			case 0x3:
				// 0x3XNN: Skip if Register X and value NN are equal
				if ( VX == IINN(inst) ) PC += 2;
				break;
			case 0x4:
				// 0x3XNN: Skip if Register X and value NN are not equal
				if ( VX != IINN(inst) ) PC += 2;
				break;
			case 0x5:
				switch (IIIN(inst)) {
					case 0:
						// 0x5XY0: Check if Register X is equal to Register Y and jump if that is
						// the case
						if (VX == VY) PC += 2;
						break;
				}
				break;
			case 0x6:
				// 0x6XNN: Set Register X
				VX = IINN(inst);
				break;
			case 0x7:
				// 0x7XNN: Add NN to the Register X, do not set the Register F
				VX += IINN(inst);
				break;
			case 0x8:
				switch (IIIN(inst)) {
					case 0:
						// 0x8XY0: Set Register X to the value of Register Y
						VX = VY;
						break;
					case 1:
						// 0x8XY1: Perform (Register X or Register Y) and save the result in
						// Register X
						VX |= VY;
						break;
					case 2:
						// 0x8XY2: Perform (Register X and Register Y) and save the result in
						// Register X
						VX &= VY;
						break;
					case 3:
						// 0x8XY3: Perform (Register X xor Register Y) and save the result in
						// Register X
						VX ^= VY;
						break;
					case 4:
						// 0x8XY4: Perform (Register X + Register Y) and save the result in
						// Register X, setting the Register F to 1 if it overflowed

						// Detect overflow before adding
						Flag = (0xFF - VX <= VY);

						// Actually add
						VX += VY;
						break;
					case 5:
						// 0x8XY5: Perform (Register X - Register Y) and save the result in
						// Register X, setting the Register F to 1 if we don't underflow (and to
						// zero otherwise)
						Flag = !(VX < VY);
						VX -= VY;
						break;
					case 6:
						// 0x8XY6: Right shift the Register X and store the result in the
						// Register X, afterwards storing the shifted bit in the Register F
						//
						// Or, to replicate old CHIP-8 behaviour, set the Register X to the
						// Register Y and then do the above
						if (ShiftQuirk) {
							VX = VY;
						}

						// Set the flag to the first bit
						Flag = VX & 1;

						VX >>= 1;

						break;
					case 7:
						// 0x8XY7: Perform (Register Y - Register X) and save the result in
						// Register X, setting the Register F to 1 if we don't underflow (and to
						// zero otherwise)
						Flag = !(VX > VY);
						VX = VY - VX;
						break;
					case 0xE:
						// 0x8XYE: Left shift the Register X and store the result in the
						// Register X, afterwards storing the shifted bit in the Register F
						//
						// Or, to replicate old CHIP-8 behaviour, set the Register X to the
						// Register Y and then do the above
						if (ShiftQuirk) {
							VX = VY;
						}

						// Set the flag to the last bit
						Flag = VX & 0x80;

						VX <<= 1;

						break;
				}
				break;
			case 0x9:
				switch (IIIN(inst)) {
					case 0:
						// 0x9XY0: Skip an instruction if the Register X does not have the same
						// value as the Register Y
						if ( VX != VY ) PC += 2;
						break;
				}
				break;
			case 0xA:
				// 0xANNN: Set the I pointer to NNN
				IPointer = INNN(inst);
				break;
			case 0xB:
				// 0xBNNN: Jump to address NNN + the value of the Register 0
				// 0xBXNN in SCHIP: Jump to adress XNN + the value of Register X

				if (JOffsetQuirk) {
					PC = INNN(inst) + VX - 1;
				} else {
					PC = INNN(inst) + Register[0] - 1;
				}
				break;
			case 0xC:
				// 0xCXNN: Get a random number, AND it with NN and put it in the Register X
				VX = ( (uint8_t)random() ) & IINN(inst);
				break;
			case 0xD: {
				// 0xDXYN: Draw a N pixels tall sprite from the memory location I, at the
				// coordinate {Register X, Register Y}, the bits drawn are XOR'ed and if
				// one gets set to 0 then the Register F gets set to 1, otherwise it is set to 0

				uint16_t x = VX & (SCREEN_WIDTH-1);
				uint16_t y = VY & (SCREEN_HEIGHT-1);

				Flag = 0;

				uint16_t mem_loc = IPointer;
				uint16_t xi;
				uint16_t yi;
				uint8_t draw;
				uint8_t pix;

				// Draw column
				for (yi = y; yi < (y+IIIN(inst)) && yi < SCREEN_HEIGHT; yi++, mem_loc++) {
					// Draw row
					// The way we decode the pixel is by right shifting it and ANDing it with
					// 1, getting a single bit which corresponds to the current pixel
					for (xi = x, draw = RAM[mem_loc]; xi < SCREEN_WIDTH && draw > 0; xi++, draw <<= 1) {
						// Check if current pixel/bit is 0 or 1
						pix = draw & 0x80;

						// Set the Register F to 1 if the XOR would result in 1
						Flag = Screen[y][x] && pix;

						// XOR the pixel
						Screen[yi][xi] ^= pix;
					}
				}

				redraw();
				break;
			}
			case 0xE:
				switch (IINN(inst)) {
					case 0x9E:
						// 0xEX9E: Skip one instruction if the key corresponding to the value in
						// the Register X is pressed
						if ( Keys[VX & 15] == true ) PC += 2;
						break;
					case 0xA1:
						// 0xEXA1: Skip one instruction if the key corresponding to the value in
						// the Register X is not pressed
						if ( Keys[VX & 15] == false ) PC += 2;
						break;
				}
				break;
			case 0xF:
				switch (IINN(inst)) {
					case 0x07:
						// 0xFX07: Set the Register X to the current value of the delay timer
						VX = DelayTimer;
						break;
					case 0x0A: {
						// 0xFX0A: Wait until a key is pressed and put its number in the
						// Register X
						bool out = false;
						for (size_t i = 0; i < 16; i++) {
							if (Keys[i] == true) {
								out = true;
								break;
							}
						}
						if (out == false) PC--;
						break;
					}
					case 0x15:
						// 0xFX15: Set the delay timer to the Register X
						DelayTimer = VX;
						break;
					case 0x18:
						// 0xFX18: Set the sound timer to the Register X
						SoundTimer = VX;
						break;
					case 0x1E:
						// 0xFX1E: Add the value of the Register X to the I pointer, if the
						// quirk is enabled, set the Register F to 1 if I overflowed and to
						// 0 otherwise
						if (Add2IndexQuirk) {
							Flag = ( 0xFFF - VX <= IPointer );
						}

						IPointer += VX;
						break;
					case 0x29:
						// 0xFX29: Set the I pointer to the corresponding font character pointed
						// by the Register X
						IPointer = FONT_START + (5 * IIIN(VX));
						break;
					case 0x33:
						// 0xFX33: Perform BCD on the Register X and store the 100s in I,
						// the 10s in I+1 and the 1s in I+2
						RAM[IPointer] = VX / 100; // 100s
						RAM[IPointer+1] = (VX / 10) % 10; // 10s
						RAM[IPointer+2] = VX % 10; // 1s
						break;
					case 0x55: {
						// 0xFX55: Store the Registers 0 to X (inclusive) in I + the Register
						// number
						if (SaveLoadQuirk) {
							uint16_t index = 0;

							for (; index < INII(inst) && index < RAM_AMOUNT; index++) {
								RAM[IPointer + index] = Register[index];
							}
						} else {
							for (; IPointer < INII(inst) && IPointer < RAM_AMOUNT; IPointer++) {
								RAM[IPointer] = Register[IIIN(IPointer)];
							}
						}
						break;
					}
					case 0x65: {
						// 0xFX65: Load the Registers 0 to X from I + the Register number
						if (SaveLoadQuirk) {
							uint16_t index = 0;

							for (; index < INII(inst) && index < RAM_AMOUNT; index++) {
								Register[index] = RAM[IPointer + index];
							}
						} else {
							for (; IPointer < INII(inst) && IPointer < RAM_AMOUNT; IPointer++) {
								Register[IIIN(IPointer)] = RAM[IPointer];
							}
						}
						break;
					}
				}
				break;
		}
		// Sleep for the specified time
		select(NULL, NULL, NULL, NULL, ms)
	}

	tb_shutdown();
}
